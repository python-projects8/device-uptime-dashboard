import global_vars
from config import load_config
from ping_hosts import ping_host_list
from time import sleep
from json import load, dumps
from hashlib import sha256
from getpass import getpass


def update_host_status() -> None:
    load_config()
    while True:
        global_vars.host_status = ping_host_list(global_vars.config["hosts"])
        sleep(5)


def validate_user(username, password):
    user_info = None
    with open("users.json", "r") as f:
        user_info = load(f)
    if username in user_info:
        return sha256(password.encode()).hexdigest() == user_info[username]


def add_user(username, password):
    user_info = None
    with open("users.json", "r") as f:
        user_info = load(f)
    user_info[username] = sha256(password.encode()).hexdigest()
    with open("users.json", "w") as f:
        f.write(dumps(user_info, indent=3))


def check_for_users():
    user_info = None
    with open("users.json", "r") as f:
        user_info = load(f)
    if len(list(user_info.keys())) == 0:
        print("No login credentials found. Please provide login credentials.")
        username = input("Username: ")

        password = None
        while True:
            password = getpass()
            conf = getpass("Confirm Password: ")
            if password == conf:
                break
            print("passwords do not match. Please try again.")

        add_user(username, password)
