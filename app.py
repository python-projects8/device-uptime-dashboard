from global_vars import app
from functions import update_host_status, check_for_users
from threading import Thread
from config import load_config
import views

if __name__ == '__main__':
    check_for_users()
    load_config()
    Thread(target=update_host_status).start()
    app.run(debug=True)
