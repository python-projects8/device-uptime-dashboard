import global_vars
from config import load_config, save_config
from flask import request, render_template, redirect, session
from connector import get_connector, get_configuration, build_empty_configuration, save_connectors
import secrets
from functions import validate_user

global_vars.app.secret_key = secrets.token_bytes(256)


@global_vars.app.route('/login', methods=["GET", "POST"])
def login():
    if session.get("username"):
        return redirect("/")
    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")
        if validate_user(username, password):
            session["username"] = username
            return redirect("/")
        return render_template("login.html", error_message="Login Failed")

    return render_template("login.html")


@global_vars.app.route('/')
def index():
    if not session.get("username"):
        return render_template("login.html", error_message="Permission denied. Please log in.")
    return render_template("index.html", results=global_vars.host_status)


@global_vars.app.route('/hosts')
def hosts():
    if not session.get("username"):
        return render_template("login.html", error_message="Permission denied. Please log in.")
    return {"hosts": global_vars.host_status}, 200


@global_vars.app.route('/config/hosts', methods=["GET", "POST"])
def host_config():
    if not session.get("username"):
        return render_template("login.html", error_message="Permission denied. Please log in.")
    # If method is post, save config
    if request.method == "POST":
        for key, value in request.form.items():
            new_key, index = key.split(".")
            while len(global_vars.config["hosts"]) <= index:
                global_vars.config["hosts"].append({})
            global_vars.config["hosts"][index]["new_key"] = value
        save_config(global_vars.config)

    # Get current config and return
    load_config()
    return render_template("host_config.html", results=global_vars.config)


@global_vars.app.route('/config/connectors')
def connector_config():
    if not session.get("username"):
        return render_template("login.html", error_message="Permission denied. Please log in.")
    return render_template("connector_config.html", connectors=global_vars.connectors)


@global_vars.app.route('/config/connectors/<connector_name>')
@global_vars.app.route('/config/connectors/<connector_name>/<configuration_name>', methods=["GET", "POST"])
def single_connector_config(connector_name: str, configuration_name=None):
    if not session.get("username"):
        return render_template("login.html", error_message="Permission denied. Please log in.")
    error_message = None
    success_message = None
    new_config = False
    connector = get_connector(connector_name=connector_name)
    current_configuration = None
    if configuration_name and configuration_name != '-New Configuration-':
        current_configuration = get_configuration(
            connector_name=connector_name, configuration_name=configuration_name)
    else:
        new_config = True
        current_configuration = build_empty_configuration(
            connector_name=connector_name)

    if request.method == "POST":
        for key, value in request.form.items():
            current_configuration[key] = value
        if new_config:
            connector["configurations"].append(current_configuration)
        save_connectors()
        success_message = "Saved successfully"
        load_config()

    return render_template('single_connector_config.html', connector=connector, current_configuration=current_configuration, success_message=success_message, error_message=error_message)


@global_vars.app.route('/config/connectors/<connector_name>/<configuration_name>/delete')
def delete_connector_config(connector_name: str, configuration_name: str):
    if not session.get("username"):
        return render_template("login.html", error_message="Permission denied. Please log in.")
    connector = get_connector(connector_name=connector_name)
    configuration = get_configuration(connector_name, configuration_name)
    connector["configurations"].remove(configuration)
    save_connectors()
    return redirect("/config/connectors/%s" % connector_name)


@global_vars.app.route('/saveconfig', methods=["POST"])
def saveconfig():
    if not session.get("username"):
        return render_template("login.html", error_message="Permission denied. Please log in.")
    save_config(request.get_json(force=True))
    return {"message": "success"}, 200


@global_vars.app.route('/saveconnector/<connector_name>', methods=["POST"])
def save_connector(connector_name):
    if not session.get("username"):
        return render_template("login.html", error_message="Permission denied. Please log in.")
    # get activation and name from form
    active = request.form.get("active") != None

    # change in connectors global variable
    connector = get_connector(connector_name=connector_name)
    connector["active"] = active

    # save connectors
    save_connectors()

    # reload config
    load_config()

    return redirect("/config/connectors")
