from loguru import logger
from flask import Flask
from sys import stdout, stderr
from threading import Lock

app = Flask(__name__)

config = None

host_status = []

connectors = []

host_status_lock = Lock()

logger.remove()
logger.add(stdout, level="WARNING")

