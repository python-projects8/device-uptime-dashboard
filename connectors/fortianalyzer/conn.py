import requests
import json

def get_config(config:dict):
    return config["username"], config["password"], config["host"]

def login(config:dict) -> str:
    username, password, host = get_config(config)
    body = {
        "method": "exec",
        "params": [
            {
                "data": {
                    "passwd": password,
                    "user": username
            },
            "url": "/sys/login/user"
            }
        ]
    }
    results = requests.post(url="https://%s/jsonrpc" % host, json=body)
    return results.json()["session"]

def get_hosts(config:dict):
    body = {
    "method": "get",
    "params": [
        {
            "fields": [
                "ip",
                "name"
            ],
            "url": "/dvmdb/device"
        }
    ],
    "session": login(config),
    "id": 1
    }
    results = requests.post(url="https://%s/jsonrpc" % config["host"], json=body)
    hosts = results.json()["result"][0]["data"]
    hosts = filter(lambda a: ("FG" in a["name"] or "FWF" in a["name"]) and not "-SEN-" in a["name"], hosts)
    hosts = [
        {"name": host["name"], "label": host["name"], "host": host["ip"]} 
        for host in hosts
    ]
    return hosts 
    
