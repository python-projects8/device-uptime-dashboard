hosts = [
          {
               "active": True,
               "host": "1.1.1.1",
               "label": "Cloudflare DNS",
               "name": "cloudflare_dns",
               
          },
          {
               "active": True,
               "host": "8.8.8.8",
               "label": "Google DNS",
               "name": "google_dns",
               
          },
          {
               "active": True,
               "host": "redsky.fortiddns.com",
               "label": "Mike Home",
               "name": "mike_home",
               
          },
          {
               "active": True,
               "host": "3.3.3.3",
               "label": "Dead Test",
               "name": "dead_test",
               
          },
          {
               "active": True,
               "host": "google.com",
               "label": "Google",
               "name": "google",
               
          },
          {
               "active": True,
               "host": "cnn.com",
               "label": "CNN",
               "name": "cnn",
               
          },
          {
               "active": True,
               "host": "foxnews.com",
               "label": "Fox News",
               "name": "fox_news",
               
          },
          {
               "active": True,
               "host": "techcorps.org",
               "label": "Tech Corps",
               "name": "techcorps",
               
          },
          {
               "active": True,
               "host": "172.16.1.1",
               "label": "FortiGate Internal",
               "name": "fg_internal",
               
          }
     ]

def get_hosts(config):
     return hosts