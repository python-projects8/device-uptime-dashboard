import global_vars
from os import listdir
from importlib import import_module
from sys import path
from json import load, dumps


def load_connectors():
    global_vars.connectors = []
    connectors_dir = "connectors"
    d = listdir(connectors_dir)

    for f in d:
        # Import module
        connector_path = connectors_dir + "/%s" % f
        path.insert(1, connector_path)

        # Get module config
        connector_config = None
        with open("%s/config.json" % connector_path, "r") as f:
            connector_config = load(f)

        # Add module to connector list
        global_vars.connectors.append({
            "name": connector_config["name"],
            "active": connector_config["active"],
            "configuration_template": connector_config["configuration_template"],
            "configurations": connector_config["configurations"],
            "connector_path": connector_path
        })

        # If the connector is inactive, move onto the next one.
        if not connector_config["active"]:
            continue

        # Add the get_hosts function to connector
        module = import_module("conn")
        global_vars.connectors[-1]["get_hosts"] = module.get_hosts

        # Get hosts and append to host list
        for configuration in connector_config["configurations"]:
            new_hosts = module.get_hosts(configuration)
            for new_host in new_hosts:
                for field in global_vars.config["schema"]:
                    if new_host.get(field["name"]) == None:
                        new_host[field["name"]] = field["default"]
                new_host["type"] = "connector:%s:%s" % (
                    connector_config["name"], configuration["name"])
                global_vars.config["hosts"].append(new_host)


def save_connectors():
    for connector in global_vars.connectors:
        tmp = connector.copy()
        if tmp.get("get_hosts"):
            del tmp["get_hosts"]
        with open("%s/config.json" % connector["connector_path"], "w") as f:
            f.write(dumps(tmp, indent=5))


def get_connector(connector_name: str) -> dict:
    for connector in global_vars.connectors:
        if connector["name"] == connector_name:
            return connector


def get_configuration(connector_name: str, configuration_name: str) -> dict:
    connector = get_connector(connector_name)
    for config in connector["configurations"]:
        if config["name"] == configuration_name:
            return config


def get_empty_field_from_type(field_type: str):
    if field_type == "text":
        return ""
    elif field_type == "checkbox":
        return True
    else:
        return ""


def build_empty_configuration(connector_name: str) -> dict:
    connector = get_connector(connector_name)
    empty_config = {}
    for field in connector["configuration_template"]:
        empty_config[field["name"]] = get_empty_field_from_type(field["type"])
    empty_config["name"] = "-New Configuration-"
    return empty_config
