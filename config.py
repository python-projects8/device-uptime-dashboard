import global_vars
from json import load, dumps
from connector import load_connectors


def load_config() -> dict:
    with open("config.json", "r") as f:
        try:
            global_vars.config = load(f)
        except Exception as err:
            global_vars.logger.critical(
                "FATAL ERROR while reading global_vars.config file: %s" % err)

    # Save all manual entries
    manual_hosts = list(
        filter(lambda host: host["type"] == "manual", global_vars.config["hosts"]))

    # Clear hosts but keep manuals
    global_vars.config["hosts"] = manual_hosts

    # Load Connectors
    load_connectors()

    # Remove any duplicate values
    deduplicate_hosts()

    # Ensure each host entry has the necessary key/value pairs
    # If they do not exist, they will be added as the default value
    # specified in the host config "schema"
    for key in global_vars.config["schema"]:
        for host in global_vars.config["hosts"]:
            if host.get(key["name"]) == None:
                host[key["name"]] = key["default"]

    # Resave the config, as it was just changed above
    save_config(global_vars.config["hosts"])


def save_config(new_config: dict) -> None:
    global_vars.config["hosts"] = new_config
    with open("config.json", "w") as f:
        try:
            f.write(dumps(global_vars.config, indent=5))
        except Exception as err:
            global_vars.logger.error(
                "Faild to save global_vars.config: %s" % err)


def deduplicate_hosts():
    seen_hosts = []
    new_hosts = []
    for index, host in enumerate(global_vars.config["hosts"]):
        if host["host"] not in seen_hosts:
            new_hosts.append(host)
            seen_hosts.append(host["host"])
    global_vars.config["hosts"] = new_hosts
