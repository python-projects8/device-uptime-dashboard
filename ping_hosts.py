from os import popen as run
import json
from threading import Thread, Lock
from sys import platform



results_lock = Lock()

def get_cmd(host:str) -> str:
    os_cmds = {
        "linux": "ping %s -c 1 -w 1" % host,
        "win32": "ping %s -n 1 -w 1" % host
    }
    return os_cmds[platform]

def ping_host(host:str, output:dict) -> dict:
    cmd = get_cmd(host["host"])
    response = run(cmd).read()
    host["alive"] = "bytes from" in str(response)
    results_lock.acquire()
    output.append(host)
    results_lock.release()

def ping_host_list(hosts:list) -> list:
    results = []
    threads = []
    for host in hosts:
        threads.append(Thread(target=ping_host, args=(host,results,)))
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()
    return results

def get_test_list(i_max, j_max, k_max, l_max):
    test = []
    for i in range(1, i_max):
        for j in range(1, j_max):
            for k in range(1, k_max):
                for l in range(1, l_max):
                    test.append({"host": "%d.%d.%d.%d" % (i, j, k, l)})
    return test

if __name__ == "__main__":
    tests = get_test_list(9, 9, 9, 9)
    from datetime import datetime
    start = datetime.now()
    results = ping_host_list(tests)
    finish = datetime.now()
    print("Finished %d pings in %d.%d seconds" % (len(tests), (finish-start).seconds, int((finish-start).microseconds/1000) ))